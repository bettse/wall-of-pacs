import "./Credential.css";

const HEX = 0x10;

const formats = [
  {
    bits: 26,
    name: "Standard 26bit",
    format: "H10301",
    company: "HID",
    layout: [
      "X",
      "FC",
      "FC",
      "FC",
      "FC",
      "FC",
      "FC",
      "FC",
      "FC",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "X",
    ],
  },
  {
    bits: 26,
    name: "Indala 26bit",
    format: "",
    company: "HID",
    layout: [
      "X",
      "FC",
      "FC",
      "FC",
      "FC",
      "FC",
      "FC",
      "FC",
      "FC",
      "FC",
      "FC",
      "FC",
      "FC",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "X",
    ],
  },
  {
    bits: 35,
    name: "Corporate 1000",
    format: "",
    company: "HID",
    layout: [
      "X",
      "X",
      "FC",
      "FC",
      "FC",
      "FC",
      "FC",
      "FC",
      "FC",
      "FC",
      "FC",
      "FC",
      "FC",
      "FC",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "X",
    ],
  },
  {
    bits: 37,
    name: "Farpointe with Site Code",
    format: "H10304",
    company: "HID",
    layout: [
      "X",
      "FC",
      "FC",
      "FC",
      "FC",
      "FC",
      "FC",
      "FC",
      "FC",
      "FC",
      "FC",
      "FC",
      "FC",
      "FC",
      "FC",
      "FC",
      "FC",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "X",
    ],
  },
  {
    bits: 37,
    name: "37 bit",
    format: "H10302",
    company: "HID",
    layout: [
      "X",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "X",
    ],
  },
  {
    bits: 37,
    name: "generic 37bit",
    format: "",
    company: "HID",
    layout: [
      "X",
      "X",
      "X",
      "X",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "CN",
      "X",
    ],
  },
];

function Credential(props) {
  const { bits, hex } = props;
  const val = parseInt(hex, HEX);
  const binary = val.toString(2).padStart(bits, "0");
  const formatCondidates = formats.filter((f) => f.bits === bits);

  return (
    <>
      <div>
        <span>{bits}bits:&nbsp;</span>
        <pre>0x{hex}</pre>
      </div>
      <div>
        {formatCondidates.length === 0 ? (
          <div className="d-flex justify-content-end">
            <pre>{binary}</pre>
          </div>
        ) : null}
        {formatCondidates.map((fc) => (
          <div key={fc.name} className="d-flex justify-content-between">
            {fc.format.length === 0 ? (
              <span className="h4 pr-2">{fc.name}:</span>
            ) : (
              <span className="h4 pr-2">
                {fc.name}({fc.format}):
              </span>
            )}
            <span>
              {binary.split("").map((b, index) => (
                <pre key={index} className={fc.layout[index]}>
                  {b}
                </pre>
              ))}
            </span>
          </div>
        ))}
      </div>
    </>
  );
}

export default Credential;

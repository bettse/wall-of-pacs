import React, { useEffect } from "react";

import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Spinner from "react-bootstrap/Spinner";
import Alert from "react-bootstrap/Alert";
import ListGroup from "react-bootstrap/ListGroup";

import { Switch, Case } from "react-if";
import { useLocalStorage } from "react-use";

import useSubscription from './useSubscription';
import Header from "./Header";
import Footer from "./Footer";
import Credential from "./Credential";

import "./App.css";

const CREDENTIALS_TOPIC = `credentials/seen`;

const STATUS = {
  CONNECTED: "Connected",
  OFFLINE: "Offline",
  RECONNECTING: "Reconnecting",
  CLOSED: "Closed",
};

function App() {
  const { message, connectionStatus } = useSubscription(CREDENTIALS_TOPIC);
  const [credentials, setCredentials] = useLocalStorage("credentials", []);

  useEffect(() => {
    if (!message) {
      return;
    }
    const { topic, message: incoming } = message;

    if (topic === CREDENTIALS_TOPIC) {
      const { type, payload } = incoming;
      if (!payload) {
        return;
      }
      console.log('Credential', payload);
      const { hex, bits, timestamp } = payload;
      const newCredentials = [...credentials];

      const index = newCredentials.findIndex((cred) => cred.hex === hex);
      if (index === -1) {
        newCredentials.push({
          type,
          hex,
          bits,
          timestamps: [timestamp],
        });
      } else {
        newCredentials[index].timestamps.push(timestamp);
      }

      newCredentials.sort(
        (a, b) => Math.max(...b.timestamps) - Math.max(...a.timestamps)
      );
      setCredentials(newCredentials);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [message]);

  return (
    <div className="App">
      <Header connectionStatus={connectionStatus} />
      <Container fluid>
        <Row className="d-flex justify-content-center pt-5">
          <Col></Col>
          <Col>
            <Switch>
              <Case condition={connectionStatus === STATUS.CLOSED}>Closed</Case>
              <Case condition={connectionStatus === STATUS.OFFLINE}>
                <div className="text-center">
                  <Alert variant="danger">Offline</Alert>
                </div>
              </Case>
              <Case condition={connectionStatus === STATUS.RECONNECTING}>
                <Spinner animation="border" role="status">
                  Reconnecting
                </Spinner>
              </Case>
              <Case condition={connectionStatus === STATUS.CONNECTED}>
                {credentials.length === 0 ? (
                  <p>Waiting for credentials</p>
                ) : (
                  <ListGroup>
                    {credentials.map((c, index) => (
                      <ListGroup.Item key={index} className="display-5 h3">
                        <Credential bits={c.bits} hex={c.hex} />
                      </ListGroup.Item>
                    ))}
                  </ListGroup>
                )}
              </Case>
            </Switch>
          </Col>
          <Col></Col>
        </Row>
      </Container>
      <Footer />
    </div>
  );
}

export default App;

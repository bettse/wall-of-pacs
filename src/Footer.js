
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';

function Footer() {
  return (
    <footer className="footer font-small mx-auto pt-5">
      <Container fluid className="text-center">
        <Row>
          <Col>
            <small className="text-muted">
              Hosted with <a href="https://www.netlify.com/">Netlify</a>
            </small>
          </Col>
          <Col>
            <small className="text-muted">
              Using <a href="https://github.com/Red-Team-Alliance/Odo">Odo</a>
            </small>
          </Col>
        </Row>
      </Container>
    </footer>
  );
}


export default Footer;

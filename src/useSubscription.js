import { useContext, useEffect, useCallback, useState } from 'react';
import { matches } from 'mqtt-pattern';
import { MqttContext } from "mqtt-react-hooks";

// https://github.com/VictorHAS/mqtt-react-hooks/issues/54

export default function useSubscription(
  topic,
  options = {},
) {
  const { client, connectionStatus, parserMethod } = useContext(
    MqttContext,
  );

  const [message, setMessage] = useState(undefined);
  const [subscribed, setSubscribed] = useState(false);

  const subscribe = useCallback(() => {
    client?.subscribe(topic, options);
    setSubscribed(true);
  }, [client, options, topic]);

  const callback = useCallback(
    (receivedTopic, receivedMessage, cb = () => {}) => {
      const message = parserMethod?.(receivedMessage) || receivedMessage.toString();
      if ([topic].flat().some(rTopic => matches(rTopic, receivedTopic))) {
        setMessage({
          topic: receivedTopic,
          message,
        });
      }
      // Allow for rerender
      setTimeout(cb, 0);
    },
    [parserMethod, topic],
  );

  useEffect(() => {
    if (client?.connected) {
      if (!subscribed) {
        subscribe();
        client.handleMessage = (packet, cb) => {
          const { topic, payload } = packet;
          callback(topic, payload, cb);
        }
      }
    }
  }, [client, subscribe, subscribed, callback]);

  return {
    client,
    topic,
    message,
    connectionStatus,
  };
}

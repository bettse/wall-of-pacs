import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

function Header(props) {
  const { connectionStatus } = props;

  return (
    <Navbar collapseOnSelect expand="sm" bg="warning" variant="light">
      <Navbar.Brand>Wall of PACS</Navbar.Brand>
      <Navbar.Toggle aria-controls="responsive-navbar-nav" />
      <Navbar.Collapse id="responsive-navbar-nav">
        <Nav className="d-flex flex-row">
        {connectionStatus}
        </Nav>
      </Navbar.Collapse>
    </Navbar>

  );
}


export default Header;

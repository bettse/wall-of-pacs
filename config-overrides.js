const webpack = require('webpack');

module.exports = function override(config, env) {
	if (!config.resolve.fallback) {
		config.resolve.fallback = {};
	}

  config.resolve.fallback['url'] = require.resolve("url/");

	if (!config.plugins) {
		config.plugins = [];
	}

	config.plugins.push(
    new webpack.ProvidePlugin({ process: 'process/browser', Buffer: ['buffer', 'Buffer'] })
  );

	return config;
};
